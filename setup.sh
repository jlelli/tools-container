#!/bin/bash

UNAME=$(uname -r)

mkdir /usr/lib/debug/lib/modules/
#ln -s /host/usr/src/kernels/${UNAME} /lib/modules/${UNAME}/build
ln -s /host/lib/modules/${UNAME} /lib/modules/
ln -s /host/usr/lib/debug/lib/modules/${UNAME} /usr/lib/debug/lib/modules/
ln -s /usr/share/bcc/tools bcc-tools
mount -t debugfs none /sys/kernel/debug/
